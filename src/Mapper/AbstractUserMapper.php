<?php
namespace App\Mapper;

/**
 * Class AbstractUserMapper
 * @package App\Mapper
 */
abstract class AbstractUserMapper
{
    /**
     * @param array $providerUser
     * @return array
     */
    abstract function mapUser(array $providerUser): array;

    /**
     * @param $providerResponse
     * @return array
     */
    public function convertToLocalUsers($providerResponse): array
    {
        $data = $providerResponse['data'];
        $users = [];

        foreach ($data as $providerUser) {
            $users[] = $this->mapUser($providerUser);
        }

        return $users;
    }

    /**
     * @param $providerResponse
     * @return array
     */
    public function convertToLocalComments($providerResponse): array
    {
      return [];
    }
}
