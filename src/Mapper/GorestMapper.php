<?php
namespace App\Mapper;

use App\Entity\Comment;
use App\Entity\User;
use App\Service\ApiProviderFactory;

/**
 * Class GorestMapper
 * @package App\Mapper
 */
class GorestMapper extends AbstractUserMapper
{
    /**
     * @param array $providerUser
     * @return array
     */
    public function mapUser(array $providerUser): array
    {
        $user = new User();

        $user->setId($providerUser['id']);
        $user->setName($providerUser['name']);
        $user->setEmail($providerUser['email']);
        $user->setGender($providerUser['gender']);
        $user->setStatus($providerUser['status']);
        $user->setCreatedAt($providerUser['created_at']);
        $user->setProvider(ApiProviderFactory::PROVIDER_GOREST);

        return $user->jsonSerialize();
    }

    /**
     * @param $providerResponse
     * @return array
     */
    public function convertToLocalComments($providerResponse): array
    {
        $data = $providerResponse['data'];
        $comments = [];

        foreach ($data as $providerComment) {
            $comment = new Comment();
            $comment->setId($providerComment['id']);
            $comment->setPostId($providerComment['post_id']);
            $comment->setName($providerComment['name']);
            $comment->setBody($providerComment['body']);

            $comments[] = $comment->jsonSerialize();
        }

        return $comments;
    }
}
