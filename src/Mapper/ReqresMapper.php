<?php

namespace App\Mapper;

use App\Entity\User;
use App\Service\ApiProviderFactory;

/**
 * Class ReqresMapper
 * @package App\Mapper
 */
class ReqresMapper extends AbstractUserMapper
{

    /**
     * @param array $providerUser
     * @return array
     */
    public function mapUser(array $providerUser): array
    {
        $user = new User();

        $user->setId($providerUser['id']);
        $name = sprintf('%s %s', $providerUser['first_name'], $providerUser['last_name']);
        $user->setName($name);
        $user->setEmail($providerUser['email']);
        $user->setAvatar($providerUser['avatar']);
        $user->setProvider(ApiProviderFactory::PROVIDER_REQRES);

        return $user->jsonSerialize();
    }
}
