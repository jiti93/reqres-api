<?php

namespace App\Controller;

use App\Service\ApiProviderFactory;
use App\Traits\ApiResponser;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CommentsController
{
    use ApiResponser;

    /**
     * @Route("/comments", name="app_comments_index")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $apiProvider = ApiProviderFactory::create(ApiProviderFactory::PROVIDER_GOREST);

        try {
            $comments = $apiProvider->getComments();
        } catch (GuzzleException $e) {
            return $this->errorResponse($e->getMessage(), $e->getCode());
        }

        return $this->successResponse($comments);
    }
}