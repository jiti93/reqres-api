<?php
namespace App\Controller;

use App\Exception\UserNotFoundException;
use App\Traits\ApiResponser;
use App\Service\ApiProviderFactory;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Class UsersController
 * @package App\Controller
 */
class UsersController
{
    use ApiResponser;
    const PROVIDER_KEY = 'provider';

    /**
     * @Route("/users", name="app_users_index")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $provider = $request->get(self::PROVIDER_KEY, '');

        $apiProvider = ApiProviderFactory::create($provider);

        try {
            $users = $apiProvider->getUsers();
        } catch (GuzzleException $e) {
            return $this->errorResponse($e->getMessage(), $e->getCode());
        }

        return $this->successResponse($users);
    }

    /**
     * @Route("/users/{id}/{provider}", name="app_users_view")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function view(int $id, string $provider): JsonResponse
    {
        if (!$provider) {
            return $this->errorResponse('The provider is mandatory, please pass it.', JsonResponse::HTTP_BAD_REQUEST);
        }

        $apiProvider = ApiProviderFactory::create($provider);

        try {
            $user = $apiProvider->getUser($id);
        } catch (GuzzleException | UserNotFoundException $e) {
            return $this->errorResponse($e->getMessage(), $e->getCode());
        }

        return $this->successResponse($user);
    }
}
