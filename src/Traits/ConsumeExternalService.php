<?php

namespace App\Traits;

use GuzzleHttp\Client;

trait ConsumeExternalService
{
    /**
     * Send request to any service
     *
     * @param string $method
     * @param string $requestUrl
     * @param array $formParams
     * @param array $headers
     *
     * @return array
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function performRequest(
        string $method,
        string $requestUrl,
        array $formParams = [],
        array $headers = []
    ): array {
        $client = new Client([
            'base_uri' => $this->baseUri,
        ]);

        $response = $client->request($method, $requestUrl, [
            'form_params' => $formParams,
            'headers' => $headers,
        ]);

        return json_decode($response->getBody()->getContents(), true);
    }
}
