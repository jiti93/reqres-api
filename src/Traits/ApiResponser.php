<?php

namespace App\Traits;

use Symfony\Component\HttpFoundation\JsonResponse;

trait ApiResponser
{
    protected $headers = [
        'Content-Type' => 'application/json'
    ];

    /**
     * Success Response
     *
     * @param $data
     * @param int $code
     *
     * @return JsonResponse
     */
    public function successResponse($data, $code = JsonResponse::HTTP_OK): JsonResponse
    {
        $responseObj = [
            'code' => $code,
            'error' => false,
            'data' => $data
        ];

        return new JsonResponse(
            $responseObj,
            $code,
           $this->headers
        );
    }

    /**
     * @param $message
     * @param $code
     *
     * @return JsonResponse
     */
    public function errorResponse($message, $code): JsonResponse
    {
        $data = [
            'code' => $code,
            'error' => $message,
            'data' => []
        ];

        return new JsonResponse(
            $data,
            $code,
            $this->headers
        );
    }
}
