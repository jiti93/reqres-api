<?php
namespace App\Service;

/**
 * Class ApiProviderFactory
 * @package App\Service
 */
class ApiProviderFactory
{
    const PROVIDER_GOREST = 'gorest';
    const PROVIDER_REQRES = 'reqres';

    /**
     * @param string $provider
     * @return AbstractProvider
     */
    public static function create(string $provider): AbstractProvider
    {
        switch ($provider) {
            case self::PROVIDER_GOREST:
                return new GorestProviderService();
            case self::PROVIDER_REQRES:
                return new ReqresProviderService();
            default:
                return new CustomProviderService();
        }
    }
}
