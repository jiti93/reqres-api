<?php

namespace App\Service;

/**
 * Class AbstractProvider
 * @package App\Service
 */
abstract class AbstractProvider
{
    const REQUEST_TYPE_GET = "GET";

    const API_BASE_URI_GOREST = 'https://gorest.co.in';
    const API_BASE_URI_REQRES = 'https://reqres.in';

    const HOST_GOREST_USERS = 'public-api/users';
    const HOST_GOREST_COMMENTS = 'public-api/comments';
    const HOST_REQRES_USERS = 'api/users';

    public $baseUri;

    /**
     * @return GorestProviderService
     */
    protected function getGorestProvider(): GorestProviderService
    {
        return new GorestProviderService();
    }

    /**
     * @return ReqresProviderService
     */
    protected function getReqrestProvider(): ReqresProviderService
    {
        return new ReqresProviderService();
    }

    /**
     * @return array
     */
    public function getComments(): array
    {
        return [];
    }

    /**
     * @param int $userId
     * @return array
     */
    public function getUser(int $userId): array
    {
        return [];
    }

    /**
     * @return array
     */
    abstract function getUsers(): array;
}
