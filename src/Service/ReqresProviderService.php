<?php

namespace App\Service;

use App\Exception\UserNotFoundException;
use App\Mapper\ReqresMapper;
use App\Traits\ConsumeExternalService;

/**
 * Class ReqresProviderService
 * @package App\Service
 */
class ReqresProviderService extends AbstractProvider
{
    use ConsumeExternalService;

    public function __construct()
    {
        $this->baseUri = self::API_BASE_URI_REQRES;
    }

    /**
     * @return array
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getUsers(): array
    {
        $usersResponse = $this->performRequest(self::REQUEST_TYPE_GET,self::HOST_REQRES_USERS);
        $mapper = new ReqresMapper();
        return $mapper->convertToLocalUsers($usersResponse);
    }

    /**
     * @param int $userId
     *
     * @return array
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getUser(int $userId): array
    {
        $url = sprintf('%s/%d', self::HOST_REQRES_USERS, $userId);
        $userResponse = $this->performRequest(self::REQUEST_TYPE_GET,$url);

        if (!isset($userResponse['data']) || isset($userResponse['data']['message'])) {
            throw new UserNotFoundException($userResponse['data']['message'], $userResponse['code']);
        }

        $mapper = new ReqresMapper();
        return $mapper->mapUser($userResponse['data'] ?? []);
    }
}
