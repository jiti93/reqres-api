<?php

namespace App\Service;

/**
 * Class CustomProviderService
 * @package App\Service
 */
class CustomProviderService extends AbstractProvider
{

    /**
     * Combine users from Go Rest and Req Res providers
     *
     * @return array
     */
    public function getUsers(): array
    {
        $gorestProvider = $this->getGorestProvider();
        $gorestUsers = $gorestProvider->getUsers();

        $reqresProvider = $this->getReqrestProvider();
        $reqresUsers = $reqresProvider->getUsers();

        return array_merge($gorestUsers, $reqresUsers);
    }
}
