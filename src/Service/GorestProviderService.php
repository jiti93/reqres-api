<?php

namespace App\Service;

use App\Exception\UserNotFoundException;
use App\Mapper\GorestMapper;
use App\Traits\ConsumeExternalService;

/**
 * Class GorestProviderService
 * @package Service
 */
class GorestProviderService extends AbstractProvider
{
    use ConsumeExternalService;

    public function __construct()
    {
        $this->baseUri = self::API_BASE_URI_GOREST;
    }

    /**
     * @return array
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getUsers(): array
    {
        $usersResponse = $this->performRequest(self::REQUEST_TYPE_GET,self::HOST_GOREST_USERS);
        $mapper = new GorestMapper();

        return $mapper->convertToLocalUsers($usersResponse);
    }

    /**
     * @param int $userId
     *
     * @return array
     *
     * @throws UserNotFoundException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getUser(int $userId): array
    {
        $url = sprintf('%s/%d', self::HOST_GOREST_USERS, $userId);
        $userResponse = $this->performRequest(self::REQUEST_TYPE_GET,$url);

        if (!isset($userResponse['data']) || isset($userResponse['data']['message'])) {
            throw new UserNotFoundException($userResponse['data']['message'], $userResponse['code']);
        }

        $mapper = new GorestMapper();
        return $mapper->mapUser($userResponse['data'] ?? []);
    }

    /**
     * @return array
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getComments(): array
    {
        $commentsResponse = $this->performRequest(self::REQUEST_TYPE_GET, self::HOST_GOREST_COMMENTS);
        $mapper = new GorestMapper();
        return $mapper->convertToLocalComments($commentsResponse);
    }
}
